
package ixigo.test.com.ixigotest.repository.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class Appendix {

    @SerializedName("airlines")
    @Expose
    private Map<String, String> airlines;
    @SerializedName("airports")
    @Expose
    private Map<String, String>  airports;

    public Map<String, String> getAirlines() {
        return airlines;
    }

    public Map<String, String> getAirports() {
        return airports;
    }

    public Map<Long, String> getProviders() {
        return providers;
    }

    @SerializedName("providers")
    @Expose
    private Map<Long, String> providers;



}
