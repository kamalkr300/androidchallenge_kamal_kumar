package ixigo.test.com.ixigotest;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import ixigo.test.com.ixigotest.repository.model.Flight;
import ixigo.test.com.ixigotest.repository.model.HomePageModel;
import ixigo.test.com.ixigotest.viewmodel.HomeViewModel;

import static android.support.v7.widget.LinearLayoutManager.VERTICAL;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private HomePageAdapter homePageAdapter;
    private HomeViewModel homeViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.id_recyclerview);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(VERTICAL);
        recyclerView.setLayoutManager(llm);

        findViewById(R.id.textView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeViewModel.sortFlightOnPrice();
            }
        });

        findViewById(R.id.textView2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeViewModel.sortFlightOnTakeOff();
            }
        });

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        homeViewModel.getHomePageData();
        homeViewModel.homePageModelMutableLiveData.observe(this, new Observer<java.util.List<ixigo.test.com.ixigotest.repository.model.Flight>>() {
            @Override
            public void onChanged(@Nullable List<Flight> flights) {
                if(homePageAdapter == null) {
                    homePageAdapter = new HomePageAdapter(flights);
                    recyclerView.setAdapter(homePageAdapter);
                    return;
                }

                homePageAdapter.updateFlights(flights);
                homePageAdapter.notifyDataSetChanged();
            }
        });

    }
}
