package ixigo.test.com.ixigotest.repository.api;

import io.reactivex.Single;
import ixigo.test.com.ixigotest.repository.model.HomePageModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HomePageApi {

    @GET(".")
    Single<HomePageModel> homePageModel();
}
