package ixigo.test.com.ixigotest.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ixigo.test.com.ixigotest.repository.Repo;
import ixigo.test.com.ixigotest.repository.model.Appendix;
import ixigo.test.com.ixigotest.repository.model.Fare;
import ixigo.test.com.ixigotest.repository.model.Flight;
import ixigo.test.com.ixigotest.repository.model.HomePageModel;

public class HomeViewModel extends ViewModel {



    public MutableLiveData<List<Flight>> homePageModelMutableLiveData = new MutableLiveData<>();
    private HomePageModel mHomePageModel;

    public void getHomePageData(){
        Single<HomePageModel> homePageModelCall = Repo.getInstance().getHomePageAPI();
        homePageModelCall.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new SingleObserver<HomePageModel>() {
            @Override
            public void onSubscribe(Disposable d) {
                Log.d("resp", "subscribe");
            }

            @Override
            public void onSuccess(HomePageModel homePageModel) {
                mHomePageModel = homePageModel;

                List<Flight> flights = homePageModel.getFlights();
                Appendix appendix = homePageModel.getAppendix();
                for (int i = 0; i<flights.size();i++){
                    Flight flight = flights.get(i);
                    flight.setAirLineName(appendix.getAirlines().get(flight.getAirlineCode()));

                    List<Fare> fares = flight.getFares();
                    for (int j = 0; j<fares.size();j++){
                        Fare fare = fares.get(j);
                        fare.setProviderName(appendix.getProviders().get(fare.getProviderId()));
                    }

                }

                homePageModelMutableLiveData.setValue(homePageModel.getFlights());
            }

            @Override
            public void onError(Throwable e) {
                Log.d("resp", e.getMessage());
            }
        });
    }

    public void sortFlightOnPrice(){
        List<Flight> flights = new ArrayList<>(mHomePageModel.getFlights());

        flights.sort(new Comparator<Flight>() {
            @Override
            public int compare(Flight o1, Flight o2) {
                long o1fare = findMinFare(o1.getFares());
                long o2fare = findMinFare(o2.getFares());

                return (int) (o1fare - o2fare);
            }

            private long findMinFare(List<Fare> fares) {
                long minFare = fares.get(0).getFare();
                for(int i = 1; i < fares.size(); i++){
                    if(minFare > fares.get(i).getFare()){
                        minFare = fares.get(i).getFare();
                    }
                }
                return minFare;
            }
        });

        homePageModelMutableLiveData.setValue(flights);

    }

    public void sortFlightOnTakeOff(){
        List<Flight> flights = new ArrayList<>(mHomePageModel.getFlights());

        flights.sort(new Comparator<Flight>() {

            @Override
            public int compare(Flight o1, Flight o2) {
                return (int) (o1.getDepartureTime() - o2.getDepartureTime());
            }
        });

        homePageModelMutableLiveData.setValue(flights);

    }



}
