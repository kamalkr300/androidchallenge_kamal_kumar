package ixigo.test.com.ixigotest;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import ixigo.test.com.ixigotest.repository.model.Fare;
import ixigo.test.com.ixigotest.repository.model.Flight;

class HomePageAdapter extends RecyclerView.Adapter<HomePageAdapter.FlightViewHolder> {
    private List<Flight> flightList;

    public HomePageAdapter(List<Flight> homePageModel) {
        this.flightList = homePageModel;
    }

    @NonNull
    @Override
    public FlightViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new FlightViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.flight_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FlightViewHolder flightViewHolder, int i) {
        flightViewHolder.bind(flightList.get(i));
    }


    @Override
    public int getItemCount() {
        return flightList.size();
    }

    public void updateFlights(List<Flight> flights) {
        this.flightList = flights;
    }

    class FlightViewHolder extends RecyclerView.ViewHolder {
        private final TextView dest;
        private final TextView deptt;
        private final TextView arrival;
        private final TextView classTv;
        private final TextView faresTv;
        private TextView origin;

        public FlightViewHolder(@NonNull View itemView) {
            super(itemView);

            origin = itemView.findViewById(R.id.origin_textV);
            dest = itemView.findViewById(R.id.dest_textV);
            deptt = itemView.findViewById(R.id.deptt_tv);
            arrival = itemView.findViewById(R.id.arrival_tv);
            classTv = itemView.findViewById(R.id.class_tv);
            faresTv = itemView.findViewById(R.id.fares_tv);


        }

        public void bind(Flight flight) {
            dest.setText(flight.getDestinationCode());
            origin.setText(flight.getOriginCode());
            deptt.setText(getTimeInMillis(flight.getDepartureTime()));
            arrival.setText(getTimeInMillis(flight.getArrivalTime()));

            List<Fare> fares = flight.getFares();
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i< fares.size(); i++){
                Fare fare = fares.get(i);
                builder.append(fare.getProviderName() );
                builder.append(" : ");
                builder.append(fare.getFare());
                builder.append("\n");
            }

            faresTv.setText(builder.toString());


            classTv.setText(flight.getClass_());
        }

        private String getTimeInMillis(Long timeInMillis) {
            Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(timeInMillis);

            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:MM");
            String strDate= formatter.format(instance.getTime());
            return strDate;
        }


    }
}
