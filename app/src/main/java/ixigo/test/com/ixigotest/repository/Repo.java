package ixigo.test.com.ixigotest.repository;

import java.io.IOException;

import io.reactivex.Single;
import ixigo.test.com.ixigotest.repository.api.HomePageApi;
import ixigo.test.com.ixigotest.repository.model.HomePageModel;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

public class Repo {

    private static Repo repo;

    public static Repo getInstance(){
        if(repo == null){
            repo = new Repo();
        }
        return repo;
    }

    private Repo()
    { }


    private  Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();

    public Single<HomePageModel> getHomePageAPI() {
        HomePageApi service = retrofit.create(HomePageApi.class);
        return service.homePageModel();
    }


}
